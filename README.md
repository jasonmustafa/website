# Website

Personal website for showcasing programming projects.

## Built With

* [Bulma](https://bulma.io/) - CSS Framework
* [Animate.css](https://daneden.github.io/animate.css/) - CSS Animation Library

## Authors

* **Jason Mustafa**